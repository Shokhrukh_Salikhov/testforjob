package com.example.demo.controller;

import com.example.demo.entity.Employee;
import com.example.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public List<Employee> employeeList(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "20") Integer size
    ){
        Pageable pageable = PageRequest.of(page, size);
        return employeeService.findAll(pageable);
    }

    @RequestMapping(path = "/create", method = RequestMethod.POST)
    public Employee employeeCreate(
            @RequestParam(name = "name") String name,
            MultipartFile file
    ) {
        return employeeService.create(name, file);
    }

    @RequestMapping(path = "/view", method = RequestMethod.GET)
    public Employee employeeView(
            @RequestParam(name = "id") Integer id
    ){
        return employeeService.findById(id);
    }

    @RequestMapping(path = "/edit", method = RequestMethod.POST)
    public Employee employeeEdit(
            @RequestParam(name = "id") Integer id,
            @RequestParam(name = "name") String name,
            MultipartFile file
    ){
        return employeeService.edit(id, name, file);
    }

    @RequestMapping(path = "delete", method = RequestMethod.POST)
    public HashMap<String, Object> employeeDelete(
            @RequestParam(name = "id") Integer id
    ){
        HashMap<String, Object> answer = new HashMap<>();
        answer.put("status", "success");
        Integer result = employeeService.delete(id);
        if(result < 0){
            answer.put("status", "error");
            answer.put("error", "Employee did not find");
        }
        return answer;
    }

}

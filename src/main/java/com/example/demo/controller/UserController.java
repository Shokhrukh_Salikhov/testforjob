package com.example.demo.controller;

import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/reg", method = RequestMethod.POST)
    public HashMap<String, Object> registration(
            @RequestParam(name = "username") String username,
            @RequestParam(name = "password") String password
    ){
        HashMap<String, Object> answer = new HashMap<>();
        answer.put("status", "Success");

        Integer result = userService.checkInputs(username, password);
        if(result < 0){
            answer.put("status", "Error");
            if(result == -1)
                answer.put("error", "Username must be only latin characters, numbers and special symbols");
            if(result == -2)
                answer.put("error", "Password must have at least 6 symbols");
            if(result == -3)
                answer.put("error", "Password must be only latin characters, numbers and special symbols");
            return answer;
        }

        userService.createUser(username, password);
        return answer;
    }

    @RequestMapping(value = "/auth", method = RequestMethod.GET)
    public HashMap<String, Object> login(
            @RequestParam(name = "username") String username,
            @RequestParam(name = "password") String password,
            HttpServletResponse response
    ) throws IOException {
        HashMap<String, Object> answer = new HashMap<>();
        answer.put("status", "Success");
        Integer result = userService.checkUser(username, password);
        if(result < 0){
            answer.put("status", "Error");
            if(result == -1)
                answer.put("error", "User did not find");
            if(result == -2)
                answer.put("error", "Password incorrect");
            if(result == -3)
                answer.put("error", "Wrong input");
            return answer;
        }
        response.sendRedirect("/employee/list");
        return answer;
    }

}

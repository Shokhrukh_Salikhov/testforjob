package com.example.demo.controller;

import com.example.demo.entity.File;
import com.example.demo.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FileController {
    @Autowired
    private FileService fileService;

    @RequestMapping(value = "/photo/download", method = RequestMethod.GET)
    public ResponseEntity<Resource> photoDownload(
            @RequestParam(name = "id") Integer id
    ){
        File file = fileService.findById(id);
        if (file == null) {
            return null;
        } else {
            return fileService.getFileAsResourceForDownloading(file);
        }
    }
}

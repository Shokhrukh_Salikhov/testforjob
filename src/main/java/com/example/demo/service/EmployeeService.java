package com.example.demo.service;

import com.example.demo.entity.Employee;
import com.example.demo.entity.File;
import com.example.demo.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private FileService fileService;

    public List<Employee> findAll(Pageable pageable){
        Page<Employee> employeePage = employeeRepository.findAll(pageable);
        return employeePage.getContent();
    }

    public Employee create(String name, MultipartFile multipartFile) {
        Employee employee = new Employee();
        employee.setName(name);

        File file = fileService.uploadFile(multipartFile);
        employee.setPhoto("/photo/download?id=" + file.getId());
        return employeeRepository.save(employee);
    }

    public Employee findById(Integer id){
        return employeeRepository.getById(id);
    }

    public Employee edit(Integer id, String name, MultipartFile multipartFile){
        Employee employee = employeeRepository.getById(id);
        if(employee == null) return null;

        employee.setName(name);
        File file = fileService.uploadFile(multipartFile);
        employee.setPhoto("/photo/download?id=" + file.getId());
        return employeeRepository.save(employee);
    }

    public Integer delete(Integer id){
        Integer result = 0; // 0 = success, -1 = employee did not find
        Employee employee = employeeRepository.getById(id);
        if(employee == null){
            result = -1;
            return result;
        }
        employeeRepository.delete(employee);
        return result;
    }
}

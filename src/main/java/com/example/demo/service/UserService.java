package com.example.demo.service;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    String regex = "^[a-zA-Z0-9%#<~|_!?,.]+$";

    public Integer checkInputs(String username, String password){
        Integer result = 0; //0 = success, -1 = error in username, -2 = not enough letters in password, -3 = error in password

        if(!username.matches(regex)){
            result = -1;
            return result; // Not need check another ifs
        }

        if(password.length() < 6){
            result = -2;
            return result;
        }

        if(!password.matches(regex)){
            result = -3;
            return result;
        }

        return result;
    }

    public User createUser(String username, String password){
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        return userRepository.save(user);
    }

    public Integer checkUser(String username, String password){
        Integer result = 0; // 0 = success, -1 = user did not find, -2 = password incorrect, -3 = wrong input

        if(!username.isEmpty()){
            User user = userRepository.findByUsername(username);
            if(user != null){
                if(!user.getPassword().equals(password)){
                    result = -2;
                }
            }
            else{
                result = -1;
            }
        }
        else{
            result = -3;
        }

        return result;
    }
}

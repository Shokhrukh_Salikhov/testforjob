package com.example.demo.service;

import com.example.demo.entity.File;
import com.example.demo.repository.FileRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

@Service
public class FileService {
    @Autowired
    private FileRepository fileRepository;

    private static Logger logger = LogManager.getLogger(FileService.class);


    public File findById(Integer id){
        return fileRepository.getOne(id);
    }

    public File save(File file){
        return fileRepository.save(file);
    }

    private Resource getFileAsResource(File file) {
        try {
            Path filePath = Paths.get(file.getPath());
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                logger.error("could not read file: " + file.getPath());

            }
        } catch (Exception e) {
            logger.error("FilesServiceImpl.getFileAsResource", e);
            e.printStackTrace();
        }
        return null;
    }

    public ResponseEntity<Resource> getFileAsResourceForDownloading(File file) {
        Resource fileAsResource = getFileAsResource(file);

        //File not found.
        if (fileAsResource == null) return null;

        MultiValueMap<String,String> headers = new LinkedMultiValueMap<>();

        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"");

        if (file.getSize() != null) {
            headers.add(HttpHeaders.CONTENT_LENGTH, file.getSize().toString());
        }
        if (file.getExtension() != null) {
            switch (file.getExtension().toLowerCase()) {
                case "pdf":
                    headers.add(HttpHeaders.CONTENT_TYPE, "application/pdf");
                    break;
                case "zip":
                    headers.add(HttpHeaders.CONTENT_TYPE, "application/zip");
                    break;
                case "rar":
                    headers.add(HttpHeaders.CONTENT_TYPE, "application/x-rar-compressed");
                    break;
                case "jpeg":
                case "jpg":
                    headers.add(HttpHeaders.CONTENT_TYPE, "image/jpeg");
                    break;
                case "png":
                    headers.add(HttpHeaders.CONTENT_TYPE, "image/png");
                    break;
                case "gif":
                    headers.add(HttpHeaders.CONTENT_TYPE, "image/gif");
                    break;
                case "doc":
                    headers.add(HttpHeaders.CONTENT_TYPE, "application/msword");
                    break;
                case "docx":
                    headers.add(HttpHeaders.CONTENT_TYPE, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                    break;
                case "xls":
                    headers.add(HttpHeaders.CONTENT_TYPE, "application/vnd.ms-excel");
                    break;
                case "xlsx":
                    headers.add(HttpHeaders.CONTENT_TYPE, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    break;
                default:
                    //Noma'lum fayl turlari uchun shu tip to'g'ri keladi
                    headers.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");
            }
        }
        return new ResponseEntity<Resource>(fileAsResource, headers, HttpStatus.OK);
    }

    public File uploadFile(MultipartFile multipartFile) {

        if (multipartFile == null || multipartFile.isEmpty()) {
            return null;
        }
        try {
            Date date = new Date();
            Long dataLong = date.getTime();

            String extension = getExtensionFromFileName(multipartFile.getOriginalFilename());

            String filename = dataLong + "." + extension;
            String directory = getPathForUpload();
            Path filePath = Paths.get(directory, filename);

            // save the file localy
            Files.copy(multipartFile.getInputStream(), filePath);

            File fileEntity = new File();

            fileEntity.setName(multipartFile.getOriginalFilename());
            fileEntity.setExtension(extension);
            fileEntity.setSize(Integer.valueOf(String.valueOf(multipartFile.getSize())));
            fileEntity.setPath(directory + "/" + filename);

            return fileRepository.saveAndFlush(fileEntity);
        } catch (Exception e) {
            logger.error("FilesServiceImpl.uploadFile", e);
        }
        return null;
    }

    private String getExtensionFromFileName(String fileName) {
        String extension = fileName;
        int i = extension.lastIndexOf('.');
        if (i >= 0) {
            extension = extension.substring(i + 1);
        } else {
            extension = "";
        }
        return extension;
    }

    private String currentDir = "";

    public synchronized String getPathForUpload() {
        createAndSetNextUploadFolder();

        return currentDir;
    }

    private void createAndSetNextUploadFolder() {
        String newCurrentDir = "D://uploads";
        java.io.File root = new java.io.File(newCurrentDir);
        if (!root.exists() || !root.isDirectory()) {
            root.mkdirs();
        }
        currentDir = newCurrentDir;
    }
}
